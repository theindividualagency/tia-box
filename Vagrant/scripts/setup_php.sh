# setup_php.sh
#
# This file sets up our PHP
PHP_VERSION=${1}

    # Choose our suPHP PHP version
    echo 'Setting our PHP version'
    sudo sed -i "s#/php\([0-9]\{2\}\)/#/php$PHP_VERSION/#g" /etc/suphp.conf
    sudo ln -sf /usr/bin/php$PHP_VERSION /usr/bin/php

    # Restarting apache
    echo 'Restarting apache'
    sudo apachectl restart