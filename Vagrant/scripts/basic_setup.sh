# basic_setup.sh
#
# This file manages the basic config for the server, such as domain name and
# admin email address
HOST=${1}
ADMIN_EMAIL=${2}

    # Server Domain
    echo 'Updating server domain'
    sudo cp /dev/null /etc/hosts
    sudo printf "127.0.0.1 $HOST\n::1       $HOST" >> /etc/hosts
    sudo sed -i "s/::1       \(.*\)/::1       $HOST/g" /etc/hosts
    sudo sed -i "s/HOSTNAME=\(.*\)/HOSTNAME=$HOST/g" /etc/sysconfig/network
    sudo sed -i "s/ServerName \(.*\):80/ServerName $HOST:80/g" /usr/local/apache2/conf/httpd.conf
    sudo sed -i "s/ServerName \(.*\):443/ServerName $HOST:443/g" /usr/local/apache2/conf/extra/httpd-ssl.conf
    sudo sed -i "s/ServerName \(.*\)/ServerName $HOST/g" /usr/local/apache2/conf/extra/httpd-non-ssl.conf
    sudo sed -i "s#Redirect \"/\" \"https://\(.*\)\"#Redirect \"/\" \"https://$HOST\"#g" /usr/local/apache2/conf/extra/httpd-non-ssl.conf
    sudo hostname $HOST > /dev/null 2>&1
    sudo /etc/init.d/network restart > /dev/null 2>&1

    # Server Email Address
    echo 'Updating server admin email address'
    sudo sed -i "s/ServerAdmin \(.*\)/ServerAdmin $ADMIN_EMAIL/g" /usr/local/apache2/conf/httpd.conf
    sudo sed -i "s/ServerAdmin \(.*\)/ServerAdmin $ADMIN_EMAIL/g" /usr/local/apache2/conf/extra/httpd-ssl.conf
