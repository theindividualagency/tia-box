# import_db.sh
#
# This file imports the DB
TARGET_DB=${1}

    # Importing DB
    echo "Creating database and importing $TARGET_DB.sql"
    mysql -u root --password=vagrant -e "CREATE DATABASE \`$TARGET_DB\`"
    mysql -u root --password=vagrant $TARGET_DB < /usr/local/apache2/db/$TARGET_DB.sql