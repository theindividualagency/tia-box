# generate_ssl.sh
#
# This file generates an SSL for our hostname
HOST=${1}

    # Generate the SSL
    echo 'Generating SSL certificate'
    sudo openssl genrsa -out /etc/ssl/private/apache-selfsigned.key 3072 > /dev/null 2>&1
    sudo openssl req -new -x509 -key /etc/ssl/private/apache-selfsigned.key -sha256 -out /etc/ssl/certs/apache-selfsigned.pem -days 730 -subj "/C=GB/ST=West Yorkshire/L=Leeds/O=TIA/OU=Digital/CN=$HOST" > /dev/null 2>&1
    sudo cat /etc/ssl/certs/dhparam.pem | sudo tee -a /etc/ssl/certs/apache-selfsigned.pem > /dev/null 2>&1

    # Restarting apache
    echo 'Restarting apache'
    sudo apachectl restart