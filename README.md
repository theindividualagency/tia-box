# TIA Box
>TIA Box is a vagrant box for developing websites in an environment as close as possible to the TIA web servers. It runs on CentOS 6 with Apache and MariaDB.

## Feature List

* Apache 2.4
* MariaDB 10.1.36
* HTTP2
* Automatic HTTPS
* Multiple PHP versions
* Composer
* WP-CLI
* MailHog
* Node & NPM
* Xdebug

## How to use

### Steps

1. Download the latest version `curl -o tia.zip https://bitbucket.org/theindividualagency/tia-box/get/1.0.0.tar.gz -o tia.tar.gz`
2. Extract the files `tar -zxvf tia.zip  --strip-components=1`
3. Delete the zip file `rm tia.zip`
4. Update the configuration variables in `./Vagrant/Vagrantfile/`
5. Update the filename of `./db/{TIA_DB}.sql` to the value of the `TARGET_DB` configuration value, for example if `TARGET_DB` is `tia`, then the file should be called `tia.sql`.
6. Run `vagrant up`

### Basic Usage
Each TIA Box is intended to only contain a single website. Once the machine is running, you can access it by visiting the domain specified in the `HOST_NAME` configuration option.

You can SSH into the machine by executing `vagrant ssh` within the `./Vagrant` folder. If you need to perform and root commands, the password is `vagrant`.


### PHP
You can easily change the version of PHP you want to use by changing the `PHP_VERSION` configuration variable. Valid options are...

* `72` (_Default_, 7.2.10)
* `71` (7.1.22)
* `70` (7.0.32)

The document root can be found at `/usr/local/apache2/htdocs/`.

### MySQL

On provisioning, the vagrant box will create a database called the value of the `TARGET_DB` configuration variable and import the `./db/*.sql` file into it.

The contents of the database will also be dumped into this file on `vagrant halt` and `vagrant destroy` so you don't lose any changes.

To details for database access are as follows.

* Host: `192.168.33.10`
* Port: `3306`
* Username: `root`
* Password: `vagrant`


### MailHog
PHP's `sendmail` has been modified to use `mhsendmail` so that emails route through to 
MailHog. You can access mailhog by visiting `http://192.168.33.10:8025/`